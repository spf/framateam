#!/bin/bash

####################
#### MATTERMOST ####
####################

###
## Ajout de la nav
#
PREFIX=/opt/gitlab/embedded/service/mattermost/client
MAIN=$PREFIX/root.html
if [[ $(grep -c framasoft.org $MAIN) -eq 0 ]]
then
    sed -e "s@</body>@<script src=\"https://framasoft.org/nav/nav.js\" type=\"text/javascript\"></script></body>@" -i $MAIN

    echo "Nav ajoutée."
    MAT_RESTART=1
else
    echo "Pas besoin d'ajouter la nav, elle est déjà présente."
fi

###
## Remplacement des favicons
#
A57=$(sed -e "s@.*sizes=57x57 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-57.png $PREFIX/$A57
cp /opt/framateam/add-nav/team-57.png $PREFIX/images/favicon/apple-touch-icon-57x57.png
A60=$(sed -e "s@.*sizes=60x60 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-60.png $PREFIX/$A60
cp /opt/framateam/add-nav/team-60.png $PREFIX/images/favicon/apple-touch-icon-60x60.png
A72=$(sed -e "s@.*sizes=72x72 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-72.png $PREFIX/$A72
cp /opt/framateam/add-nav/team-72.png $PREFIX/images/favicon/apple-touch-icon-72x72.png
A76=$(sed -e "s@.*sizes=76x76 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-76.png $PREFIX/$A76
cp /opt/framateam/add-nav/team-76.png $PREFIX/images/favicon/apple-touch-icon-76x76.png
A144=$(sed -e "s@.*sizes=144x144 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-144.png $PREFIX/$A144
cp /opt/framateam/add-nav/team-144.png $PREFIX/images/favicon/apple-touch-icon-144x144.png
A120=$(sed -e "s@.*sizes=120x120 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-120.png $PREFIX/$A120
cp /opt/framateam/add-nav/team-120.png $PREFIX/images/favicon/apple-touch-icon-120x120.png
A152=$(sed -e "s@.*sizes=152x152 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-152.png $PREFIX/$A152
cp /opt/framateam/add-nav/team-152.png $PREFIX/images/favicon/apple-touch-icon-152x152.png

F16=$(sed -e "s@.*sizes=16x16 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-16.png $PREFIX/$F16
cp /opt/framateam/add-nav/team-16.png $PREFIX/images/favicon/favicon-16x16.png
F32=$(sed -e "s@.*sizes=32x32 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-32.png $PREFIX/$F32
cp /opt/framateam/add-nav/team-32.png $PREFIX/images/favicon/favicon-32x32.png
F96=$(sed -e "s@.*sizes=96x96 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-96.png $PREFIX/$F96
cp /opt/framateam/add-nav/team-96.png $PREFIX/images/favicon/favicon-96x96.png
F192=$(sed -e "s@.*sizes=192x192 href=/static/\([^>]\+\).*@\1@" $PREFIX/root.html)
cp /opt/framateam/add-nav/team-192.png $PREFIX/$F192
cp /opt/framateam/add-nav/team-192.png $PREFIX/images/favicon/android-chrome-192x192.png

################
#### GITLAB ####
################

###
##  Sélection de l'onglet de login standard par défaut
#
sed -e "s@, class: active_when(i.zero? && \!crowd_enabled?)@@" -e "s@.login-box.tab-pane{ id: 'ldap-standard', role: 'tabpanel' }@.login-box.tab-pane.active{id: 'ldap-standard', role: 'tabpanel'}@" -i /opt/gitlab/embedded/service/gitlab-rails/app/views/devise/shared/_signin_box.html.haml
sed -e "s@li{ class: active_when(i.zero? && \!crowd_enabled?) }@li@" -e "/password_authentication_enabled_for_web?\$/,/ldap-standard/ s@li\$@li.active@" -i /opt/gitlab/embedded/service/gitlab-rails/app/views/devise/shared/_tabs_ldap.html.haml

/usr/bin/gitlab-ctl restart unicorn

###
##  Contournement d'un bug
#
chmod 644 /opt/gitlab/embedded/service/gitlab-rails/.gitlab_workhorse_secret

###############
#### Nginx ####
###############
NGINX_RESTART=0

###
## Framateam
#
if [[ $(grep -c acme-challenge /var/opt/gitlab/nginx/conf/gitlab-mattermost-http.conf) -eq 0 ]]
then
    NGINX_RESTART=1
    sed -i -e "s@gitlab_mattermost_error.log;@gitlab_mattermost_error.log;\n\n  location ^~ '/.well-known/acme-challenge' {\n    default_type \"text/plain\";\n    root /opt/certbot;\n  }@" /var/opt/gitlab/nginx/conf/gitlab-mattermost-http.conf
fi

###
## Framagit
#
if [[ $(grep -c acme-challenge /var/opt/gitlab/nginx/conf/gitlab-http.conf) -eq 0 ]]
then
    NGINX_RESTART=1
    sed -i -e "s@gitlab_error.log;@gitlab_error.log;\n\n  location ^~ '/.well-known/acme-challenge' {\n    default_type \"text/plain\";\n    root /opt/certbot;\n  }@"  /var/opt/gitlab/nginx/conf/gitlab-http.conf
fi

if [[ $NGINX_RESTART -eq 1 ]]
then
    /usr/bin/gitlab-ctl restart nginx
fi
