.. Mattermost documentation master file, created by
   sphinx-quickstart on Thu Nov 19 13:21:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Framateam
---------
Framateam_ est un service de *tchat* libre qui permet de communiquer avec son équipe en notifiant ses collègues, de conserver ses conversations et d'y faire des recherches.

Le service repose sur le logiciel libre Mattermost_.

Voici une documentation pour vous aider à mettre en page vos messages, utiliser des emoticônes, partager des images et documents et maîtriser Framateam.
Il s’agit de la traduction française de la `documentation officielle`_ réalisé par l'équipe Framalang_


-----

.. _Framateam: https://framateam.org

.. _Mattermost: http://mattermost.org

.. _documentation officielle: https://docs.mattermost.com

.. _Framalang: https://participer.framasoft.org/traduction-rejoignez-framalang/

.. toctree::
   :maxdepth: 1
   :caption: Premiers pas
   :glob:

   help/getting-started/signing-in.md
   help/getting-started/messaging-basics.md
   help/getting-started/configuring-notifications.md
   help/getting-started/organizing-conversations.md
   help/getting-started/searching.md
   help/getting-started/creating-teams.md
   help/getting-started/managing-members.md
   help/getting-started/*

.. toctree::
   :maxdepth: 1
   :caption: Messagerie
   :glob:

   help/messaging/sending-messages.md
   help/messaging/mentioning-teammates.md
   help/messaging/formatting-text.md
   help/messaging/attaching-files.md
   help/messaging/executing-commands.md
   help/messaging/*

.. toctree::
   :maxdepth: 1
   :caption: Paramètres
   :glob:

   help/settings/account-settings.md
   help/settings/theme-colors.md
   help/settings/channel-settings.md
   help/settings/team-settings.md
   help/settings/*



